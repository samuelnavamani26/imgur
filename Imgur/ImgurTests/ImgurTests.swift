//
//  ImgurTests.swift
//  ImgurTests
//
//  Created by Samuel Navamani on 30/6/18.
//  Copyright © 2018 samuel. All rights reserved.
//

import XCTest
@testable import Imgur

class ImgurTests: XCTestCase {
    
    var imgur: Imgur!
    var imgurDetails: [ImgurDetails]!
    var imgurImage: ImgurImage!
    var imgurDetail1: ImgurDetails!
    var imgurDetail2: ImgurDetails!
    
    override func setUp() {
        super.setUp()
        let dateIn = Date(timeIntervalSince1970: TimeInterval(100.00))
        imgurDetail1 = ImgurDetails(title: "TitleTest1", date: dateIn, imagesCount: 1, images: nil, isEvenNumbered: true, points: 10, score: 10, topic_id: 10)
        
        imgurImage = ImgurImage(id: "imageID", link: "https://google.com")
        imgurDetail2 = ImgurDetails(title: "TitleTest2", date: dateIn, imagesCount: 1, images: [imgurImage], isEvenNumbered: false, points: 10, score: 10, topic_id: 11)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testImgurValues() {
        //Given
        imgurDetails = [imgurDetail1, imgurDetail2]
        imgur = Imgur(status: 200, data: imgurDetails)
        
        //Then
        XCTAssertEqual(imgur.status, 200)
        XCTAssertEqual(imgur.data[0].title, "TitleTest1")
        XCTAssertEqual(imgur.data[0].isEvenNumbered, true)
        XCTAssertEqual(imgur.data[1].images![0].id, "imageID")
    }
    
    func testImgurDecodeValues() {
        //Given
        //let imgur = try? JSONDecoder().decode(Imgur.self, from: json)
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "ImgurDataSuccess", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        let imgur = try? JSONDecoder().decode(Imgur.self, from: data!)
        
        //Then
        XCTAssertEqual(imgur!.status, 200)
        XCTAssertEqual(imgur!.data[0].title, "I hope this is the final four")
    }
}
