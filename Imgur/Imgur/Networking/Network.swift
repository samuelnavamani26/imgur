//
//  Network.swift
//  Imgur
//
//  Created by Samuel Navamani on 28/6/18.
//  Copyright © 2018 ImgurSearch. All rights reserved.
//

import Foundation

/// Network class to fetch the imgur details and imgur images for the list.
class Network: NSObject, URLSessionDelegate  {
    
    typealias Result = (Imgur?, String) -> ()
    typealias ImageResult = (Data?, String) -> ()
    
    var defaultSession: URLSession? = nil
    var dataTask: URLSessionDataTask?
    
    var imgur: Imgur?
    var errorMessage = ""
    
    /// Get the imgur details for the search text.
    ///
    /// - Parameter searchText: Text to be searched for imgur.
    /// - completion: Result with imgur and/or error message.
    func getImgurDetails(for searchText: String, completion: @escaping Result) {
        dataTask?.cancel()
        
        if searchText.isEmpty { return }
        
        defaultSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        
        if let url = URL(string: "https://api.imgur.com/3/gallery/search/?q=\(searchText)") {
            
            var request = URLRequest(url: url)
            request.setValue("Client-ID 99e371b41b29080", forHTTPHeaderField: "Authorization")
            request.httpMethod = "GET"
            
            dataTask = defaultSession?.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    self.errorMessage = "Network error - \(error.localizedDescription)"
                } else  if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    self.decode(data: data)
                }
                
                DispatchQueue.main.async {
                    completion(self.imgur, self.errorMessage)
                }
            }
            dataTask?.resume()
        }
    }
    
    /// Get the imgur image.
    ///
    /// - Parameter link: Link to the image from which the image data is fetched.
    /// - completion: ImageResult with imgur image data and/or error message.
    func getImgurImage(for link: String, completion: @escaping ImageResult) {
        dataTask?.cancel()
        
        if link.isEmpty { return }
        
        defaultSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        var imageData: Data?
        if let url = URL(string: link) {
            let request = URLRequest(url: url)
            dataTask = defaultSession?.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    self.errorMessage = "Network Error"
                } else  if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    imageData = data
                }
                DispatchQueue.main.async {
                    completion(imageData, self.errorMessage)
                }
            }
            dataTask?.resume()
        }
    }

    // MARK: - urlSession delegate
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if let serverTrust = challenge.protectionSpace.serverTrust {
            completionHandler(.useCredential, URLCredential(trust: serverTrust))
        } else {
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }
}

extension Network {
    
    /// Decode the data to imgur.
    func decode(data: Data) {
        let decoder = JSONDecoder()
        do {
            self.imgur = try decoder.decode(Imgur.self, from: data)
            if let imgur = self.imgur {
                print(imgur.data)
            }
        } catch {
            print("Error while decoding - \(error.localizedDescription) - \(error)")
        }
    }
}















