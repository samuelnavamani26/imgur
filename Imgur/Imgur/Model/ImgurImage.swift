//
//  ImgurImage.swift
//  Imgur
//
//  Created by Samuel Navamani on 28/6/18.
//  Copyright © 2018 ImgurSearch. All rights reserved.
//

import Foundation

/// Holds the imgur image data from the search gallery service call.
struct ImgurImage: Decodable {
    
    /// Holds the id of the imgur image.
    let id: String
    
    /// Holds the link of the imgur image.
    let link: String
}
