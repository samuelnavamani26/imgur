//
//  ImgurDetails.swift
//  Imgur
//
//  Created by Samuel Navamani on 28/6/18.
//  Copyright © 2018 ImgurSearch. All rights reserved.
//

import Foundation

/// Holds the imgur details from the search gallery service call.
struct ImgurDetails: Decodable {
    
    /// Holds the title of the imgur data.
    let title: String
    
    /// Holds the date of the imgur data.
    let date: Date
    
    /// Holds the image count of the imgur data if available.
    let imagesCount: Int?
    
    /// Holds the array of imgur images if available.
    let images: [ImgurImage]?
    
    /// Holds if the imgur data is even numbered(ie. (points+score+topic_id)/2 ?? )
    let isEvenNumbered: Bool
    
    /// Holds the points of from the imgur data.
    let points: Int
    
    /// Holds the score of the imgur data.
    let score: Int
    
    /// Holds the topic_id of the imgur data
    let topic_id: Int
    
    enum CodingKeys: String, CodingKey {
        case title
        case date = "datetime"
        case imagesCount = "images_count"
        case images
        case points
        case score
        case topic_id
    }
}

extension ImgurDetails {
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        date = try container.decode(Date.self, forKey: .date)
        imagesCount = try container.decodeIfPresent(Int.self, forKey: .imagesCount)
        images = try container.decodeIfPresent([ImgurImage].self, forKey: .images)
        points = try container.decode(Int.self, forKey: .points)
        score = try container.decode(Int.self, forKey: .score)
        topic_id = try container.decode(Int.self, forKey: .topic_id)
        
        /// Logic to update the isEvenNumbered boolean value.
        let p = (points+score+topic_id)%2
        isEvenNumbered = (p==0) ? true : false
    }
}
