//
//  Imgur.swift
//  Imgur
//
//  Created by Samuel Navamani on 30/6/18.
//  Copyright © 2018 samuel. All rights reserved.
//

import Foundation

/// Holds the response from the search gallery imgur api.
struct Imgur: Decodable {
    
    /// Holds the status of the service call.
    let status: Int
    
    /// Holds the imgur details data.
    let data: [ImgurDetails]
}
