//
//  ViewController.swift
//  Imgur
//
//  Created by Samuel Navamani on 28/6/18.
//  Copyright © 2018 ImgurSearch. All rights reserved.
//

import UIKit

/// View controller to present the imgur search with search bar and the toggle button.
class ViewController: UIViewController {
    
    /// Table view for our view controller to display search results.
    @IBOutlet var imgurTable: UITableView! {
        didSet {
            imgurTable.delegate = self
            imgurTable.dataSource = self
        }
    }

    /// Search bar for our view controller to allow users to search images.
    @IBOutlet var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    
    /// Activity indicator to show progress while searching images.
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    /// Switch to toggle between even numbered data to be displayed.
    @IBOutlet var evenSwitch: UISwitch!
    
    /// Date formatter to format the display of dates.
    var formatter = DateFormatter()
    
    /// Network layer to connect to the required api service.
    let network = Network()
    
    /// Property to hold all the imgur details from the service call.
    var imgurDetails: [ImgurDetails]?
    
    /// Holds the images loaded and displayed.
    var cachedImages = [String: UIImage]()
    
    /// Placeholder image from the assets to be displayed when there is no image from the api sercice.
    let imagePlaceholder: UIImage! = UIImage(named: "imageplaceholder")
    
    // MARK: - Lifecyle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        evenSwitch.addTarget(self, action: #selector(stateChanged), for: .valueChanged)
        
        formatter.dateFormat = "dd-MMM-yyyy"
        
        imgurTable.rowHeight = UITableViewAutomaticDimension
        imgurTable.estimatedRowHeight = 140
        
        searchBar.becomeFirstResponder()
    }
}

// MARK: - View controller with table view data source and delegate.
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let imgurDetail = imgurDetails, imgurDetail.count > 0 {
            return imgurDetail.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImgurCell", for: indexPath) as! ImgurTableViewCell
        
        if let imgurDetail = imgurDetails, imgurDetail.count > 0 { // Check if imgur details available
            let imgur = imgurDetail[indexPath.row]
            cell.title.text = imgur.title
            cell.date.text = formatter.string(from: imgur.date)
            
            if let imageCount = imgur.imagesCount {
                cell.noOfImages.text = "No of Images: \(imageCount)"
            } else {
                cell.noOfImages.text = "No of Images: 0"
            }
            
            if let image = imgur.images { // Get image links to fetch the image.
                if !image[0].link.isEmpty {
                    let imageURL = image[0].link
                    if let image = cachedImages[imageURL] {
                        cell.imgurImageView.image = image
                    } else {
                        DispatchQueue.global(qos: .userInteractive).async {
                            self.network.getImgurImage(for: imageURL) { imageData, error in
                                if !error.isEmpty {
                                    print("ImageURL - \(imageURL) for title - \(imgur.title) not loaded due to error - \(error)")
                                }
                                
                                DispatchQueue.main.async {
                                    if let data = imageData, let image = UIImage(data: data) {
                                        self.cachedImages[imageURL] = image
                                        cell.imgurImageView.image = image
                                        self.imgurTable.reloadData()
                                    } else {
                                        cell.imgurImageView.image = self.imagePlaceholder
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                cell.imgurImageView.image = imagePlaceholder
            }
        } else {
            cell.title.text = ""
            cell.date.text = ""
            cell.date.text = "No search result"
            cell.imgurImageView.image = nil
            
        }
        return cell
    }
}

// MARK: - UISearchBarDelegate
extension ViewController: UISearchBarDelegate {
    
    fileprivate func searchImgurDetails() {
        if let searchText = searchBar.text, searchText != "" {
            activityIndicator.startAnimating()
            
            DispatchQueue.global(qos: .userInteractive).async {
                self.network.getImgurDetails(for: searchText) { results, errorMessage in
                    if let result = results {
                        self.imgurDetails = result.data
                        if self.evenSwitch.isOn {
                            if let imgurDetails = self.imgurDetails {
                                let evenImgurDetails = imgurDetails.filter({ (imgurDetail) -> Bool in
                                    return imgurDetail.isEvenNumbered == true
                                }).sorted(by: { (details1, details2) -> Bool in
                                    return details1.date.compare(details2.date) == .orderedDescending
                                })
                                self.imgurDetails = evenImgurDetails
                            }
                        }
                    }
                    
                    if !errorMessage.isEmpty {
                        print("Search button clicked error - \(errorMessage)")
                    }
                    
                    DispatchQueue.main.async {
                        self.imgurTable.reloadData()
                        self.activityIndicator.stopAnimating()
                    }
                }
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchImgurDetails()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

// MARK: - UISwitch
extension ViewController {
    
    @objc func stateChanged(state: UISwitch) {
        searchImgurDetails()
    }
}

