//
//  ImgurTableViewCell.swift
//  Imgur
//
//  Created by Samuel Navamani on 2/7/18.
//  Copyright © 2018 samuel. All rights reserved.
//

import UIKit

/// TableViewCell to show the imgur details.
class ImgurTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var noOfImages: UILabel!
    @IBOutlet weak var imgurImageView: UIImageView!
}
