# Imgur

## Project

This project is written in swift for searching image from imgur api and displays the details on table view.

## Configurations


It supports 2 targets. 

Imgur - Runs the project on simulator.
ImgurTests - Runs the unit test for the project.
